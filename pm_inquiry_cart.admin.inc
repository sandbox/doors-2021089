<?php

/**
 * @file
 * Administration page callbacks for the Private Message Inquiry Cart module.
 */

/**
 * Select the Content Types and URL for the Inquiry Cart's page.
 */
function pm_inquiry_cart_admin_settings() {
  // Get an array of node types with internal names as keys and
  // "friendly names" as values. E.g.,
  // array('page' => ’Basic Page, 'article' => 'Articles')
  $types = node_type_get_types();
  foreach ($types as $node_type) {
    $options[$node_type->type] = $node_type->name;
  }

  $form['pm_inquiry_cart_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content Types'),
    '#options' => $options,
    '#default_value' => variable_get('pm_inquiry_cart_node_types', array('page')),
    '#description' => t('Select all the content types that you want to persons to be able to inquire about.'),
  );

  $form['pm_inquiry_cart_url'] = array(
    '#type' 			=> 'textfield',
    '#title' 			=> t('URL'),
    '#default_value' => variable_get('pm_inquiry_cart_url'),
    '#description' 	=> t('Enter the URL to be used for the Inquiry Cart page.'),
    '#weight'      	=> 0,
  );

  return system_settings_form($form);
}
