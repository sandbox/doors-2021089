
-- SUMMARY --

This module integrates with Private Message to create an Inquiry Cart
solution that uses the Flag module to create a listing of nodes into
a Cart page.


-- REQUIREMENTS --

Private Meessage (https://drupal.org/project/privatemsg)
Flag (https://drupal.org/project/flag)


-- INSTALLATION --




-- CONFIGURATION --
